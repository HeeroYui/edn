/**
 * @author Edouard DUPIN
 * 
 * @copyright 2010, Edouard DUPIN, all right reserved
 * 
 * @license GPL v3 (see license file)
 */

#ifndef __APPL_TEXT_PLUGIN_SELECT_ALL_H__
#define __APPL_TEXT_PLUGIN_SELECT_ALL_H__

#include <etk/types.h>
#include <ewol/object/Object.h>
#include <appl/Gui/TextViewer.h>
#include <ewol/compositing/Text.h>
#include <appl/TextPlugin.h>

namespace appl {
	class TextPluginSelectAll : public appl::TextViewerPlugin {
		protected:
			TextPluginSelectAll();
			void init();
			int32_t m_menuIdTitle;
			int32_t m_menuIdSelectAll;
			int32_t m_menuIdSelectNone;
		public:
			DECLARE_FACTORY(TextPluginSelectAll);
			virtual ~TextPluginSelectAll() {
				// nothing to do ...
			};
		public:
			virtual void onPluginEnable(appl::TextViewer& _textDrawer);
			virtual void onPluginDisable(appl::TextViewer& _textDrawer);
			virtual bool onReceiveShortCut(appl::TextViewer& _textDrawer, const std::string& _shortCutName);
	};
};


#endif
